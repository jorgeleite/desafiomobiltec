﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Model
{
    public class Veiculo
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public DateTime DataQuilometragem { get; set; }
        public long Quilometragem { get; set; }
        public decimal CombustivelLitragem { get; set; }
        public decimal PrecoLitroCombustivel { get; set; }
    }
}
