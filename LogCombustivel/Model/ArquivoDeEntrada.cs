﻿using LogCombustivel.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Model
{
    
    public class ArquivoDeEntrada

    {
        public string NomeDoArquivo { get; set; }

        public List<string> Cabecalhos {get;set;}
        public List<Veiculo> Veiculos { get; set; }

        public ArquivoDeEntrada(string nomeArquivo) 
        {
            Cabecalhos = new List<string>();

            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.marca));
            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.modelo));
            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.data));
            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.quilometragem));
            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.combustivel));
            Cabecalhos.Add(StringEnum.GetStringValue(EnumCabecalho.preco));

            Veiculos = new List<Veiculo>();
        
        }

    }
}
