﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Model
{
    public class ArquivoDeSaida
    {

        public List<Veiculo> ConsumosPorVeiculo{get;set;}
        


        public ArquivoDeSaida(List<Veiculo> consumos) 
        {
            ConsumosPorVeiculo = consumos;
        }


        /// <summary>
        /// A quilometragem total no período (Km)
        /// </summary>
        public long QuilometragemTotalPeriodo { get; set; }

        /// <summary>
        /// O custo total com combustível no período (R$)
        /// </summary>
        public decimal CustoTotalCombustivel { get; set; }

        /// <summary>
        /// O consumo total de combustível no período (Litros)
        /// </summary>
        public decimal ConsumoTotalCombustivel { get; set; }

        /// <summary>
        /// Data inicial do período
        /// </summary>
        public DateTime DataInicialPeriodo { get; set; }

        /// <summary>
        /// total de dias de consumo no periodo
        /// </summary>
        public long TotalDeDiasPeriodo { get; set; }

        /// <summary>
        /// O consumo médio de combustível (Km/L)
        /// </summary>
        public decimal ConsumoMedioCombustivel { get; set; }

        /// <summary>
        /// O melhor registro de consumo (Km/L)
        /// </summary>
        public decimal MelhorRegistroConsumo { get; set; }


        /// <summary>
        /// O pior registro de consumo (Km/L)
        /// </summary>
        public decimal PiorRegistroConsumo { get; set; }

        /// <summary>
        /// O custo médio por quilômetro rodado (R$ / Km) 
        /// </summary>
        public decimal CustoMedio { get; set; }
    }
}
