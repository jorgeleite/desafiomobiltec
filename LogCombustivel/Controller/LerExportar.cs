﻿using LogCombustivel.Model;
using LogCombustivel.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Controller
{
    public class LerExportar
    {
        private  ArquivoDeEntrada ArquivoEntrada;
        public string Excecao { get; set; }
        public int QuantidadeLinhasArquivo { get; set; }
        public ArquivoDeEntrada ArquivoEntradaCompleto { get; set; }
        


        #region Leitura


        public bool LeituraParcial(string arquivoParaLeitura) 
        {

            try
            {

                StreamReader arquivo = new StreamReader(arquivoParaLeitura);
                var linha="";

                while ((linha = arquivo.ReadLine()) != null)
                {
                    QuantidadeLinhasArquivo++;
                }
                                 

                arquivo.Close();

                return true;
            }
            catch (Exception ex) 
            {
                Excecao = String.Format("Ocorreu um erro ao tentar efetuar a leitura do arquivo: {0}, tente novamente ou contate o administador do sistema!  ", arquivoParaLeitura);
                return false;
            }

        
        }

        public bool LerArquivo(string arquivoParaLeitura) 
        {
            ArquivoEntrada = new ArquivoDeEntrada(arquivoParaLeitura);

            try
            {
                StreamReader arquivo = new StreamReader(arquivoParaLeitura);
                string linha = null;
                string[] linhaseparada = null;
                int contadorLinhas = 0;
               

                while ((linha = arquivo.ReadLine()) != null)
                {                     
                    
                    linhaseparada = linha.Split(',');

                    if (contadorLinhas > 0) 
                    {

                        var marca = RemoverContrabarra(linhaseparada[0]);
                        var modelo = RemoverContrabarra(linhaseparada[1]);
                        var dataQuilometragem = RemoverContrabarra(linhaseparada[2]);
                        var quilometragem = RemoverContrabarra(linhaseparada[3]);
                        var combustivelLitragem = RemoverContrabarra(linhaseparada[4]);
                        var precoLitroCombustivel =RemoverContrabarra(linhaseparada[5]);

                        precoLitroCombustivel =precoLitroCombustivel.Replace('.', ',');
                        combustivelLitragem = combustivelLitragem.Replace('.', ',');


                        Veiculo veiculo=new Veiculo();
                        veiculo.Marca = marca;
                        veiculo.Modelo = modelo;
                        veiculo.DataQuilometragem = Convert.ToDateTime(dataQuilometragem);
                        veiculo.Quilometragem=Convert.ToInt64(quilometragem);
                        veiculo.CombustivelLitragem=Convert.ToDecimal(combustivelLitragem);
                        veiculo.PrecoLitroCombustivel=Convert.ToDecimal(precoLitroCombustivel);

                        ArquivoEntrada.Veiculos.Add(veiculo);
                    
                    }
                    contadorLinhas++;

                }

                arquivo.Close();
                ArquivoEntradaCompleto = ArquivoEntrada;

                return true;
            }
            catch (Exception ex) 
            {

                Excecao = String.Format("Ocorreu um erro ao tentar efetuar a leitura do arquivo: {0}, tente novamente ou contate o administador do sistema!  ", arquivoParaLeitura);
                return false;
            }

            
        
        }



        #endregion


        #region Exportação

        public void ExportarArquivo(List<ArquivoDeSaida> resultados) 
        {


            StreamWriter exportarArquivo = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "RelatorioConsumo.csv");

            StringBuilder sb=new StringBuilder();


            sb.Append(String.Format("{0}{1}{2}",'"', StringEnum.GetStringValue(EnumCabecalhoExportacao.marca),'"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.modelo), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.quilometragem), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.custototal), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.litragemtotal), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.datainicial), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.numerodedias), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.mediakmporlitro), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.piorkmporlitro), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.melhorkmporlitro), '"'));
            sb.Append(String.Format("{0}{1}{2}", ";\"", StringEnum.GetStringValue(EnumCabecalhoExportacao.valorporlitro), '"'));


            exportarArquivo.WriteLine(sb);
            

            foreach (var resultado in resultados)
            {
                sb = new StringBuilder();
             

                sb.Append(String.Format("{0}{1}{2}",'"', resultado.ConsumosPorVeiculo.FirstOrDefault().Marca,'"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.ConsumosPorVeiculo.FirstOrDefault().Modelo, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.QuilometragemTotalPeriodo, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.CustoTotalCombustivel.ToString("#,##"), '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.ConsumoTotalCombustivel, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.DataInicialPeriodo.Date.ToShortDateString(), '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.TotalDeDiasPeriodo, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.ConsumoMedioCombustivel, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.PiorRegistroConsumo, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.MelhorRegistroConsumo, '"'));
                sb.Append(String.Format("{0}{1}{2}", ";\"", resultado.CustoMedio, '"'));







                exportarArquivo.WriteLine(sb);
            }


            exportarArquivo.Close();

       

        }
        #endregion

        #region Facilitadores

        public string RemoverContrabarra(string palavra) 
        {

            palavra = palavra.Substring(1, palavra.Length - 2).Trim();

            return palavra;
        
        }


        #endregion
    }
}
