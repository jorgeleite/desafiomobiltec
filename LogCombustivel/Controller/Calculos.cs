﻿using LogCombustivel.Model;
using LogCombustivel.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Controller
{
    public class Calculos
    {

        private ArquivoDeEntrada arquivoLido;
        public List<ArquivoDeSaida> resultados;


        public Calculos(ArquivoDeEntrada arquivo)
        {
            arquivoLido = arquivo;
            resultados = new List<ArquivoDeSaida>();

            if (arquivoLido != null && arquivoLido.Veiculos.Count > 0)
            {

                var agrupamentoPorVeiculo = arquivoLido.Veiculos.GroupBy(p => p.Modelo).ToList();
                resultados = new List<ArquivoDeSaida>();


                resultados.AddRange(from consumoAgrupado in agrupamentoPorVeiculo
                                    select new ArquivoDeSaida(consumoAgrupado.Select(p => p).ToList()));

            }



        }

        public void CalcularQuilometragemTotalNoPeriodo(ArquivoDeSaida resultado)
        {

            var quilometragemMaxima = resultado.ConsumosPorVeiculo.Max(p => p.Quilometragem);
            var quilometragemMinima = resultado.ConsumosPorVeiculo.Min(p => p.Quilometragem);

            resultado.QuilometragemTotalPeriodo = quilometragemMaxima - quilometragemMinima;

        }

        public void CalcularcustoTotalCombustivelPeriodo(ArquivoDeSaida resultado)
        {


            foreach (var consumo in resultado.ConsumosPorVeiculo)
            {
                var resultadoParcial = consumo.PrecoLitroCombustivel * consumo.CombustivelLitragem;

                resultado.CustoTotalCombustivel += resultadoParcial;
            }



        }

        public void CalcularDataInicialDoPeriodoTotalDias(ArquivoDeSaida resultado)
        {


            resultado.DataInicialPeriodo = resultado.ConsumosPorVeiculo.Min(p => p.DataQuilometragem);
            var dataFinalPeriodo = resultado.ConsumosPorVeiculo.Max(p => p.DataQuilometragem);

            resultado.TotalDeDiasPeriodo = dataFinalPeriodo.Subtract(resultado.DataInicialPeriodo).Days;


        }

        public void CalcularConsumoMedioPeriodo(ArquivoDeSaida resultado)
        {

            var media = resultado.QuilometragemTotalPeriodo / resultado.ConsumosPorVeiculo.Sum(p => p.CombustivelLitragem);
            var mediaFormatada = decimal.Round(media, 2, MidpointRounding.AwayFromZero);

            resultado.ConsumoMedioCombustivel = mediaFormatada;

        }

        public void CalcularMelhorEPiorRegistroConsumo(ArquivoDeSaida resultado)
        {
            long quilometragemAnterior = 0;
            decimal litragemAnterior = 0;



            foreach (var consumo in resultado.ConsumosPorVeiculo)
            {
                if (quilometragemAnterior > 0)
                {
                    decimal calculoComparativo = (consumo.Quilometragem - quilometragemAnterior) / (consumo.CombustivelLitragem + litragemAnterior);

                    if (resultado.PiorRegistroConsumo == 0 || resultado.PiorRegistroConsumo > calculoComparativo)
                    {
                        resultado.PiorRegistroConsumo = decimal.Round(calculoComparativo, 2, MidpointRounding.AwayFromZero);

                    }


                    if (resultado.MelhorRegistroConsumo == 0 || resultado.MelhorRegistroConsumo < calculoComparativo)
                    {
                        resultado.MelhorRegistroConsumo = decimal.Round(calculoComparativo, 2, MidpointRounding.AwayFromZero);

                    }
                }

                quilometragemAnterior = consumo.Quilometragem;
                litragemAnterior = consumo.CombustivelLitragem;
            }


        }

        public void CalcularCustoMedioKmRodado(ArquivoDeSaida resultado)
        {

            resultado.CustoMedio = decimal.Round(resultado.QuilometragemTotalPeriodo / resultado.CustoTotalCombustivel, 2, MidpointRounding.AwayFromZero);

        }

        public void CalcularConsumoTotalCombustivel(ArquivoDeSaida resultado) 
        {

              resultado.ConsumoTotalCombustivel = resultado.ConsumosPorVeiculo.Sum(p => p.CombustivelLitragem);
        }


        public void EfetuarTodosCalculos()
        {

            foreach (var resultado in resultados)
            {

                CalcularQuilometragemTotalNoPeriodo(resultado);
                CalcularcustoTotalCombustivelPeriodo(resultado);
                CalcularDataInicialDoPeriodoTotalDias(resultado);
                CalcularConsumoMedioPeriodo(resultado);
                CalcularMelhorEPiorRegistroConsumo(resultado);
                CalcularCustoMedioKmRodado(resultado);
                CalcularConsumoTotalCombustivel(resultado);


            }
        }

    }
}
