﻿namespace LogCombustivel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.btnSelecionarArquivo = new System.Windows.Forms.Button();
            this.lblNomeArquivo = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.btnExportarArquivo = new System.Windows.Forms.Button();
            this.btnImportarArquivo = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.backGroundProgress = new System.ComponentModel.BackgroundWorker();
            this.lblSucessoImportacao = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(253, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Log Combustivel";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Selecione um arquivo CSV:";
            // 
            // openFile
            // 
            this.openFile.Title = "Selecione o Arquivo";
            // 
            // btnSelecionarArquivo
            // 
            this.btnSelecionarArquivo.Location = new System.Drawing.Point(37, 104);
            this.btnSelecionarArquivo.Name = "btnSelecionarArquivo";
            this.btnSelecionarArquivo.Size = new System.Drawing.Size(75, 23);
            this.btnSelecionarArquivo.TabIndex = 2;
            this.btnSelecionarArquivo.Text = "Selecionar";
            this.btnSelecionarArquivo.UseVisualStyleBackColor = true;
            this.btnSelecionarArquivo.Click += new System.EventHandler(this.btnSelecionarArquivo_Click);
            // 
            // lblNomeArquivo
            // 
            this.lblNomeArquivo.AutoSize = true;
            this.lblNomeArquivo.Location = new System.Drawing.Point(129, 109);
            this.lblNomeArquivo.Name = "lblNomeArquivo";
            this.lblNomeArquivo.Size = new System.Drawing.Size(0, 13);
            this.lblNomeArquivo.TabIndex = 3;
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(34, 130);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(0, 13);
            this.lblWarning.TabIndex = 4;
            // 
            // btnExportarArquivo
            // 
            this.btnExportarArquivo.Enabled = false;
            this.btnExportarArquivo.Location = new System.Drawing.Point(37, 217);
            this.btnExportarArquivo.Name = "btnExportarArquivo";
            this.btnExportarArquivo.Size = new System.Drawing.Size(127, 31);
            this.btnExportarArquivo.TabIndex = 5;
            this.btnExportarArquivo.Text = "Exportar Arquivo";
            this.btnExportarArquivo.UseVisualStyleBackColor = true;
            this.btnExportarArquivo.Click += new System.EventHandler(this.btnExportarArquivo_Click);
            // 
            // btnImportarArquivo
            // 
            this.btnImportarArquivo.Enabled = false;
            this.btnImportarArquivo.Location = new System.Drawing.Point(37, 165);
            this.btnImportarArquivo.Name = "btnImportarArquivo";
            this.btnImportarArquivo.Size = new System.Drawing.Size(127, 31);
            this.btnImportarArquivo.TabIndex = 6;
            this.btnImportarArquivo.Text = "Importar Arquivo";
            this.btnImportarArquivo.UseVisualStyleBackColor = true;
            this.btnImportarArquivo.Click += new System.EventHandler(this.btnImportarArquivo_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(179, 167);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(278, 23);
            this.progressBar.TabIndex = 7;
            this.progressBar.Visible = false;
            // 
            // backGroundProgress
            // 
            this.backGroundProgress.WorkerReportsProgress = true;
            this.backGroundProgress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backGroundProgress_DoWork);
            this.backGroundProgress.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backGroundProgress_ProgressChanged);
            this.backGroundProgress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backGroundProgress_RunWorkerCompleted);
            // 
            // lblSucessoImportacao
            // 
            this.lblSucessoImportacao.AutoSize = true;
            this.lblSucessoImportacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSucessoImportacao.Location = new System.Drawing.Point(229, 166);
            this.lblSucessoImportacao.Name = "lblSucessoImportacao";
            this.lblSucessoImportacao.Size = new System.Drawing.Size(60, 24);
            this.lblSucessoImportacao.TabIndex = 8;
            this.lblSucessoImportacao.Text = "label3";
            this.lblSucessoImportacao.Visible = false;
            // 
            // Form1
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 428);
            this.Controls.Add(this.lblSucessoImportacao);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnImportarArquivo);
            this.Controls.Add(this.btnExportarArquivo);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.lblNomeArquivo);
            this.Controls.Add(this.btnSelecionarArquivo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log Combustivel";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.Button btnSelecionarArquivo;
        private System.Windows.Forms.Label lblNomeArquivo;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Button btnExportarArquivo;
        private System.Windows.Forms.Button btnImportarArquivo;
        public System.ComponentModel.BackgroundWorker backGroundProgress;
        public System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblSucessoImportacao;
    }
}

