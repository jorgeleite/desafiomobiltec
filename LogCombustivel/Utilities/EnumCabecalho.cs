﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Utilities
{
    public enum EnumCabecalho
    {

        [StringValue("MARCA")]
        marca = 1,
        [StringValue("MODELO")]
        modelo = 2,
        [StringValue("DATA")]
        data = 3,
        [StringValue("QUILOMETRAGEM")]
        quilometragem = 4,
        [StringValue("COMBUSTIVEL")]
        combustivel = 5,
        [StringValue("PRECO")]
        preco = 6

    }
}
