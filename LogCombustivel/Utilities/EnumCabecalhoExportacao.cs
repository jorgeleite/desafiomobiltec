﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogCombustivel.Utilities
{
    public enum EnumCabecalhoExportacao
    {

        [StringValue("MARCA")]
        marca = 1,
        [StringValue("MODELO")]
        modelo = 2,
        [StringValue("KM")]
        quilometragem = 3,
        [StringValue("R$")]
        custototal = 4,
        [StringValue("LITROS")]
        litragemtotal = 5,
        [StringValue("DATAINI")]
        datainicial = 6,
        [StringValue("DIAS")]
        numerodedias = 7,
        [StringValue("MEDIAKM/L")]
        mediakmporlitro = 8,
        [StringValue("PIORKM/L")]
        piorkmporlitro = 9,
        [StringValue("MELHORKM/L")]
        melhorkmporlitro = 10,
        [StringValue("R$/KM")]
        valorporlitro = 11,



    }
}
