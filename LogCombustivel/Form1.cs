﻿using LogCombustivel.Controller;
using LogCombustivel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogCombustivel
{
    public partial class Form1 : Form
    {

        private LerExportar LeituraExportacao = new LerExportar();

        public Form1()
        {
            InitializeComponent();
          
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSelecionarArquivo_Click(object sender, EventArgs e)
        {

            try
            {


                


                DialogResult dialogResult = openFile.ShowDialog();

                btnImportarArquivo.Enabled = false;
                btnExportarArquivo.Enabled = false;
                lblNomeArquivo.Text = "";
                lblSucessoImportacao.Visible = false;
                progressBar.Value = 0;
                progressBar.Visible = false;

                

                if (dialogResult == DialogResult.OK)
                {
                    

                    if (!openFile.FileName.ToUpper().Contains("CSV"))
                    {              

                        MessageBox.Show( "O sistema aceita somente arquivos com extensão CSV.");       
                  
                    }
                    else
                    {

                        lblNomeArquivo.Text = openFile.FileName;
                        btnImportarArquivo.Enabled = true;
                        progressBar.Value = 0;
          
                    }
                }

            }
            catch (Exception ex) 
            {
                 MessageBox.Show( "Erro inesperado ao tentar importar o arquivo, contate o administrador!");
            }


        }

        private void btnImportarArquivo_Click(object sender, EventArgs e)
        {

            if(LeituraExportacao.LeituraParcial(openFile.FileName))
            {
                progressBar.Visible = true;
                 backGroundProgress.RunWorkerAsync();
            }         

 
        }

        //responsável disparar a alteração o valor da barra de progresso
        private void backGroundProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            


            
            var quantidadeLinhas=LeituraExportacao.QuantidadeLinhasArquivo;


            if (quantidadeLinhas <= 0)
            {
                MessageBox.Show("O arquivo que foi lido não têm linhas  a serem processadas!");
            }
            else
            {
                var quantidadePercentual = 100 / quantidadeLinhas;
                LeituraExportacao.LerArquivo(openFile.FileName);
                for (int i = 1; i <= quantidadeLinhas; i++)
                {

                    Thread.Sleep(100);

                    var percentual = i * quantidadePercentual;

                    if (percentual > 100)
                    {
                        percentual = 100;
                    }

                    backGroundProgress.ReportProgress(percentual);
                }
            }

        }

        //responsável por alterar o valor da barra de progresso
        private void backGroundProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
            progressBar.Value = e.ProgressPercentage;
           
            //this.Text = e.ProgressPercentage.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        //responsável pela ação de conclusão do processo
        private void backGroundProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (LeituraExportacao.QuantidadeLinhasArquivo <= 0)
            {
                lblSucessoImportacao.Visible = false;
                progressBar.Visible = true;
                progressBar.Value = 0;

            }
            else
            {

                progressBar.Visible = false;
                lblSucessoImportacao.Text = "Arquivo importado com sucesso.";
                lblSucessoImportacao.Visible = true;
                btnExportarArquivo.Enabled = true;
                btnImportarArquivo.Enabled = false;

            }

        }

        private void btnExportarArquivo_Click(object sender, EventArgs e)
        {
            try
            {

                Calculos calculos = new Calculos(LeituraExportacao.ArquivoEntradaCompleto);

                calculos.EfetuarTodosCalculos();

                LeituraExportacao.ExportarArquivo(calculos.resultados);

                   
               
                MessageBox.Show("O arquivo foi exportado com sucesso em:" + AppDomain.CurrentDomain.BaseDirectory);
                


            }
            catch (Exception ex) 
            {
                MessageBox.Show("Ocorreu um erro inesperado ao tentar exportar o arquivo, tente novamente ou contate o administrador!");
            }
        }




    }
}
